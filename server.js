"use strict";
require("use-strict");

/*
 * This file is part of P4CaaS.
 * 
 * P4CaaS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * P4CaaS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with P4CaaS.  If not, see <http://www.gnu.org/licenses/>.
 */

global.XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
global.XMLHttpRequest.DONE = 4;

const express = require('express'),
	bodyParser = require('body-parser'),
	app = express(),
	port = process.env.PORT || 28111;

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use('/doc', express.static(__dirname+'/doc'));

const routes = require('./api/routes/P4CaaSRoutes');
routes(app);
const server = app.listen(port);

app.use(function(req, res) {
	res.status(404).send({url: req.originalUrl + ' not found'})
});

console.log('P4CaaS API started on port: ' + port);

module.exports = server;
