/*
 * This file is part of P4CaaS.
 * 
 * P4CaaS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * P4CaaS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with P4CaaS.  If not, see <http://www.gnu.org/licenses/>.
 */

const P4C = require("pic4carto");
const CREDENTIALS = {
	mapillary: { key: "eDhQTTdnRmZJNXdYZWUwRDQxc1NwdzphZDIyNWZjZTA3YWU3ODY1" },
	flickr: { key: "1ba9e9ec7a22f9383ea5eb35a17fc853" }
};

exports.default = function(req, res) {
	res.json({ "status": "OK" });
};

/**
 * @api {get} /search/around Around a point
 * @apiDescription Retrieve all pictures around a given point, within a given radius.
 * @apiName GetSearchAround
 * @apiGroup Search
 * 
 * @apiParam {Number} lat The latitude (WGS84)
 * @apiParam {Number} lng The longitude (WGS84)
 * @apiParam {Number} radius The radius around the point, in meters
 * @apiParam {Long} [mindate] The minimum timestamp for a given time range, in milliseconds since 1st january 1970 (Epoch)
 * @apiParam {Long} [maxdate] The maximum timestamp for a given time range, in milliseconds since 1st january 1970 (Epoch)
 * @apiParam {String} [use] The list of fetchers to use (comma-separated)
 * @apiParam {String} [ignore] The list of fetchers to ignore (comma-separated)
 * 
 * @apiSuccess {String} status The response status (OK)
 * @apiSuccess {Array} pictures The retrieved pictures, see <a href="https://framagit.org/PanierAvide/Pic4Carto.js/blob/master/doc/API.md#picture">details about the Picture object</a>.
 */
exports.searchAround = function(req, res) {
	/*console.log("[SEARCH] "
					+req.query.radius
					+" meters around "
					+req.query.lat+", "+req.query.lng
					+" between TS "
					+req.query.mindate+" and "+req.query.maxdate
					+" (use "+req.query.use+" | ignore "+req.query.ignore+")");*/
	
	const picManager = new P4C.PicturesManager({ fetcherCredentials: CREDENTIALS });
	
	//Prepare event for when pictures are downloaded
	picManager.on("picsready", function(pictures) {
		res.json({ "status": "OK", "pictures": pictures });
	});
	
	picManager.on("fetcherfailed", function(fetcherId) {
		console.log("[SEARCH] Failed on fetcher "+fetcherId);
	});
	
	//Call for pictures download
	try {
		picManager.startPicsRetrievalAround(
			new P4C.LatLng(req.query.lat, req.query.lng),
			req.query.radius,
			{
				mindate: req.query.mindate ? parseInt(req.query.mindate) : undefined,
				maxdate: req.query.maxdate ? parseInt(req.query.maxdate) : undefined,
				usefetchers: req.query.use ? req.query.use.split(',') : undefined,
				ignorefetchers: req.query.ignore ? req.query.ignore.split(',') : undefined
			}
		);
	}
	catch(e) {
		res.status(500).send({ "status": "NOTOK", "error": e.toString() });
	}
};

/**
 * @api {get} /search/bbox Bounding box
 * @apiDescription Retrieve all pictures in a bounding box.
 * @apiName GetSearchBbox
 * @apiGroup Search
 * 
 * @apiParam {Number} south The minimal latitude (WGS84)
 * @apiParam {Number} west The minimal longitude (WGS84)
 * @apiParam {Number} north The maximal latitude (WGS84)
 * @apiParam {Number} east The maximal longitude (WGS84)
 * @apiParam {Long} [mindate] The minimum timestamp for a given time range, in milliseconds since 1st january 1970 (Epoch)
 * @apiParam {Long} [maxdate] The maximum timestamp for a given time range, in milliseconds since 1st january 1970 (Epoch)
 * @apiParam {String} [use] The list of fetchers to use (comma-separated)
 * @apiParam {String} [ignore] The list of fetchers to ignore (comma-separated)
 * 
 * @apiSuccess {String} status The response status (OK)
 * @apiSuccess {Array} pictures The retrieved pictures, see <a href="https://framagit.org/PanierAvide/Pic4Carto.js/blob/master/doc/API.md#picture">details about the Picture object</a>.
 */
exports.searchBbox = function(req, res) {
	/*console.log("[SEARCH] BBox "
					+req.query.south+", "+req.query.west+", "+req.query.north+", "+req.query.east
					+" between TS "
					+req.query.mindate+" and "+req.query.maxdate
					+" (use "+req.query.use+" | ignore "+req.query.ignore+")");*/
	
	const picManager = new P4C.PicturesManager({ fetcherCredentials: CREDENTIALS });
	
	//Prepare event for when pictures are downloaded
	picManager.on("picsready", function(pictures) {
		res.json({ "status": "OK", "pictures": pictures });
	});
	
	picManager.on("fetcherfailed", function(fetcherId) {
		console.log("[SEARCH] Failed on fetcher "+fetcherId);
	});
	
	//Call for pictures download
	try {
		picManager.startPicsRetrieval(
			new P4C.LatLngBounds(new P4C.LatLng(req.query.south, req.query.west), new P4C.LatLng(req.query.north, req.query.east)),
			{
				mindate: req.query.mindate ? parseInt(req.query.mindate) : undefined,
				maxdate: req.query.maxdate ? parseInt(req.query.maxdate) : undefined,
				usefetchers: req.query.use ? req.query.use.split(',') : undefined,
				ignorefetchers: req.query.ignore ? req.query.ignore.split(',') : undefined
			}
		);
	}
	catch(e) {
		res.status(500).send({ "status": "NOTOK", "error": e.toString() });
	}
};

/**
 * @api {get} /fetchers List
 * @apiDescription The list of available picture providers
 * @apiName GetFetchers
 * @apiGroup Fetchers
 * 
 * @apiSuccess {String} status The response status (OK)
 * @apiSuccess {Array} fetchers The list of fetchers
 */
exports.fetchers = function(req, res) {
	res.json({ "status": "OK", "fetchers": (new P4C.PicturesManager()).getFetcherDetails() });
};

/**
 * @api {get} /summary/bbox Bounding box
 * @apiDescription Retrieve a summary of pictures availability in a given area
 * @apiName GetSummaryBbox
 * @apiGroup Summary
 * 
 * @apiParam {Number} south The minimal latitude (WGS84)
 * @apiParam {Number} west The minimal longitude (WGS84)
 * @apiParam {Number} north The maximal latitude (WGS84)
 * @apiParam {Number} east The maximal longitude (WGS84)
 * @apiParam {Long} [mindate] The minimum timestamp for a given time range, in milliseconds since 1st january 1970 (Epoch)
 * @apiParam {Long} [maxdate] The maximum timestamp for a given time range, in milliseconds since 1st january 1970 (Epoch)
 * 
 * @apiSuccess {String} status The response status (OK)
 * @apiSuccess {Object} summary The summary of pictures availability
 * @apiSuccess {int} summary.amount How many pictures are available in the area
 * @apiSuccess {int} summary.last The last picture timestamp, in milliseconds since 1st january 1970 (Epoch)
 * @apiSuccess {boolean} summary.approxAmount Is the returned amount approximated or not
 */
exports.summaryBbox = function(req, res) {
	/*console.log("[SUMMARY] BBox "
	 *			+req.query.south+", "+req.query.west+", "+req.query.north+", "+req.query.east
	 *			+" between TS "
	 *			+req.query.mindate+" and "+req.query.maxdate;*/
	
	const picManager = new P4C.PicturesManager({ fetcherCredentials: CREDENTIALS });
	
	//Prepare event for when summary is ready
	picManager.on("summaryready", function(summary) {
		res.json({ "status": "OK", "summary": summary });
	});
	
	picManager.on("fetcherfailed", function(fetcherId) {
		console.log("[SUMMARY] Failed on fetcher "+fetcherId);
	});
	
	//Call for pictures download
	try {
		picManager.startSummaryRetrieval(
			new P4C.LatLngBounds(new P4C.LatLng(req.query.south, req.query.west), new P4C.LatLng(req.query.north, req.query.east)),
			{
				mindate: req.query.mindate ? parseInt(req.query.mindate) : undefined,
				maxdate: req.query.maxdate ? parseInt(req.query.maxdate) : undefined
			}
		);
	}
	catch(e) {
		res.status(500).send({ "status": "NOTOK", "error": e.toString() });
	}
};
