# P4CaaS

## Read-me

At start, [Pic4Carto](http://projets.pavie.info/pic4carto) is a website for browsing open-licensed, geolocated street pictures. From this website was extracted the core functionnality of retrieving pictures from several providers (Mapillary, OpenStreetCam, Flickr, WikiCommons...) as a JavaScript library called [Pic4Carto.js](https://framagit.org/PanierAvide/Pic4Carto.js), which can be integrated in various web applications.

To make reuse of pictures in various applications even easier, **P4CaaS** (Pic4Carto as a Service) was created here. It is a Rest API which can be called over HTTP requests for searching street pictures over all managed providers.

An open instance is available here (thanks to [OpenStreetMap France](http://openstreetmap.fr/)): [api-pic4carto.openstreetmap.fr](http://api-pic4carto.openstreetmap.fr).

[![Help making this possible](https://liberapay.com/assets/widgets/donate.svg)](https://liberapay.com/PanierAvide/donate)

## Examples
### All recent pictures around a point

* Query: `GET /search/around?lat=48.10275&lng=-1.67267&radius=100` ([try it](http://api-pic4carto.openstreetmap.fr/search/around?lat=48.10275&lng=-1.67267&radius=100))

### List of managed picture providers

* Query: `GET /fetchers` ([try it](http://api-pic4carto.openstreetmap.fr/fetchers))

### Summary of recent pictures availability

* Query: `GET /summary/bbox?west=-1.6804&north=48.1073&east=-1.6706&south=48.0992` ([try it](http://api-pic4carto.openstreetmap.fr/summary/bbox?west=-1.6804&north=48.1073&east=-1.6706&south=48.0992))

### All pictures in a given bounding box and time range without certain provider

* Query: `GET /search/bbox?west=-1.6804&north=48.1073&east=-1.6706&south=48.0992&mindate=1483228800000&maxdate=1496275200000&ignore=mapillary` ([try it](http://api-pic4carto.openstreetmap.fr/search/bbox?west=-1.6804&north=48.1073&east=-1.6706&south=48.0992&mindate=1483228800000&maxdate=1496275200000&ignore=mapillary))


## Build & install

If you want to run your own instance of P4CaaS, you can follow the instructions below. However, for small tests it is simpler to use the open instance.

Before installing P4CaaS, you need to install some dependencies:
* npm

To launch your own Pic4Carto API, run the following commands:
```
git clone https://framagit.org/PanierAvide/P4CaaS.git
cd P4CaaS/
npm install
npm run start
```

Here you are, you can access the API through `http://localhost:28111/`.

If you are planning to run this server in a production context, you should think of using your own API keys for picture providers. To do so, you might change the `CREDENTIALS` variable in `api/controllers/P4CaaSController.js` file.

Unit tests are available, to run them just launch `npm run test`.


## Usage

Just run HTTP queries over the API endpoint (by default `http://localhost:28111/`). Documentation can be requested on `/doc` over the endpoint (or at [OSM-FR instance](http://api-pic4carto.openstreetmap.fr/doc/)).

For several query examples, you can have a look on unit tests, available in the `test/server_test.js` file.


## Contributing

P4CaaS, as all over projects around Pic4Carto, is open source. Contributions are welcome, and here is how you can help for this project:
* Report bugs or discuss new functionalities in the [Issues tracker](https://framagit.org/PanierAvide/P4CaaS/issues).
* Write code (and tests) and create a [merge request](https://framagit.org/PanierAvide/P4CaaS/merge_requests). As this project is using [Git Flow branching model](http://nvie.com/posts/a-successful-git-branching-model/), please create requests on __develop__ or __feature/*__ branches.


## License

Copyright 2017 [Adrien PAVIE](http://pavie.info/)

See LICENSE for complete AGPL3 license.

P4CaaS is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

P4CaaS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with P4CaaS. If not, see <http://www.gnu.org/licenses/>.
