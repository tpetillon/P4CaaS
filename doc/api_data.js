define({ "api": [
  {
    "type": "get",
    "url": "/fetchers",
    "title": "List",
    "description": "<p>The list of available picture providers</p>",
    "name": "GetFetchers",
    "group": "Fetchers",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>The response status (OK)</p>"
          },
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "fetchers",
            "description": "<p>The list of fetchers</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "api/controllers/P4CaaSController.js",
    "groupTitle": "Fetchers"
  },
  {
    "type": "get",
    "url": "/search/around",
    "title": "Around a point",
    "description": "<p>Retrieve all pictures around a given point, within a given radius.</p>",
    "name": "GetSearchAround",
    "group": "Search",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "lat",
            "description": "<p>The latitude (WGS84)</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "lng",
            "description": "<p>The longitude (WGS84)</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "radius",
            "description": "<p>The radius around the point, in meters</p>"
          },
          {
            "group": "Parameter",
            "type": "Long",
            "optional": true,
            "field": "mindate",
            "description": "<p>The minimum timestamp for a given time range, in milliseconds since 1st january 1970 (Epoch)</p>"
          },
          {
            "group": "Parameter",
            "type": "Long",
            "optional": true,
            "field": "maxdate",
            "description": "<p>The maximum timestamp for a given time range, in milliseconds since 1st january 1970 (Epoch)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "use",
            "description": "<p>The list of fetchers to use (comma-separated)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "ignore",
            "description": "<p>The list of fetchers to ignore (comma-separated)</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>The response status (OK)</p>"
          },
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "pictures",
            "description": "<p>The retrieved pictures, see <a href=\"https://framagit.org/PanierAvide/Pic4Carto.js/blob/master/doc/API.md#picture\">details about the Picture object</a>.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "api/controllers/P4CaaSController.js",
    "groupTitle": "Search"
  },
  {
    "type": "get",
    "url": "/search/bbox",
    "title": "Bounding box",
    "description": "<p>Retrieve all pictures in a bounding box.</p>",
    "name": "GetSearchBbox",
    "group": "Search",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "south",
            "description": "<p>The minimal latitude (WGS84)</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "west",
            "description": "<p>The minimal longitude (WGS84)</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "north",
            "description": "<p>The maximal latitude (WGS84)</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "east",
            "description": "<p>The maximal longitude (WGS84)</p>"
          },
          {
            "group": "Parameter",
            "type": "Long",
            "optional": true,
            "field": "mindate",
            "description": "<p>The minimum timestamp for a given time range, in milliseconds since 1st january 1970 (Epoch)</p>"
          },
          {
            "group": "Parameter",
            "type": "Long",
            "optional": true,
            "field": "maxdate",
            "description": "<p>The maximum timestamp for a given time range, in milliseconds since 1st january 1970 (Epoch)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "use",
            "description": "<p>The list of fetchers to use (comma-separated)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "ignore",
            "description": "<p>The list of fetchers to ignore (comma-separated)</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>The response status (OK)</p>"
          },
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "pictures",
            "description": "<p>The retrieved pictures, see <a href=\"https://framagit.org/PanierAvide/Pic4Carto.js/blob/master/doc/API.md#picture\">details about the Picture object</a>.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "api/controllers/P4CaaSController.js",
    "groupTitle": "Search"
  },
  {
    "type": "get",
    "url": "/summary/bbox",
    "title": "Bounding box",
    "description": "<p>Retrieve a summary of pictures availability in a given area</p>",
    "name": "GetSummaryBbox",
    "group": "Summary",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "south",
            "description": "<p>The minimal latitude (WGS84)</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "west",
            "description": "<p>The minimal longitude (WGS84)</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "north",
            "description": "<p>The maximal latitude (WGS84)</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "east",
            "description": "<p>The maximal longitude (WGS84)</p>"
          },
          {
            "group": "Parameter",
            "type": "Long",
            "optional": true,
            "field": "mindate",
            "description": "<p>The minimum timestamp for a given time range, in milliseconds since 1st january 1970 (Epoch)</p>"
          },
          {
            "group": "Parameter",
            "type": "Long",
            "optional": true,
            "field": "maxdate",
            "description": "<p>The maximum timestamp for a given time range, in milliseconds since 1st january 1970 (Epoch)</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>The response status (OK)</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "summary",
            "description": "<p>The summary of pictures availability</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "summary.amount",
            "description": "<p>How many pictures are available in the area</p>"
          },
          {
            "group": "Success 200",
            "type": "int",
            "optional": false,
            "field": "summary.last",
            "description": "<p>The last picture timestamp, in milliseconds since 1st january 1970 (Epoch)</p>"
          },
          {
            "group": "Success 200",
            "type": "boolean",
            "optional": false,
            "field": "summary.approxAmount",
            "description": "<p>Is the returned amount approximated or not</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "api/controllers/P4CaaSController.js",
    "groupTitle": "Summary"
  }
] });
