define({
  "title": "Pic4Carto API Documentation",
  "name": "p4caas",
  "version": "0.1.0",
  "description": "API for retrieving all open-licensed geolocated pictures (Pic4Carto as a Service)",
  "sampleUrl": false,
  "defaultVersion": "0.0.0",
  "apidoc": "0.3.0",
  "generator": {
    "name": "apidoc",
    "time": "2017-07-04T08:43:06.007Z",
    "url": "http://apidocjs.com",
    "version": "0.17.6"
  }
});
