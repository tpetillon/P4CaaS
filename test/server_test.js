/*
 * This file is part of P4CaaS.
 * 
 * P4CaaS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * P4CaaS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with P4CaaS.  If not, see <http://www.gnu.org/licenses/>.
 */

var assert = require('assert');
var request = require('supertest');

describe('Server routes', function() {
	var server;
	
	beforeEach(function () {
		delete require.cache[require.resolve('../server')];
		server = require('../server');
	});
	afterEach(function (done) {
		server.close(done);
	});
	
	describe('/', function() {
		it("should return status on GET", function(done){
			request(server)
			.get("/")
			.expect("Content-type",/json/)
			.expect(200)
			.end(function(err, res){
				assert.equal(res.status, 200);
				assert.equal(res.body.status, "OK");
				done();
			});
		});
	});
	
	describe('/search', function() {
		it("should return status on GET", function(done){
			request(server)
			.get("/search")
			.expect("Content-type",/json/)
			.expect(200)
			.end(function(err, res){
				assert.equal(res.status, 200);
				assert.equal(res.body.status, "OK");
				done();
			});
		});
	});
	
	describe('/search/bbox', function() {
		it("should return NOTOK on GET without parameters", function(done){
			request(server)
			.get("/search/bbox")
			.expect("Content-type",/json/)
			.expect(500)
			.end(function(err, res){
				assert.equal(res.status, 500);
				assert.equal(res.body.status, "NOTOK");
				done();
			});
		});
		
		it("should return pictures on GET with BBox only", function(done){
			request(server)
			.get("/search/bbox?south=48.125&north=48.13&west=-1.69&east=-1.685")
			.expect("Content-type",/json/)
			.expect(200)
			.end(function(err, res){
				assert.equal(res.status, 200);
				assert.equal(res.body.status, "OK");
				assert.ok(res.body.pictures.length > 0);
				assert.ok(res.body.pictures[0].pictureUrl.startsWith("http"));
				assert.ok(res.body.pictures[0].date > 0);
				assert.ok(res.body.pictures[0].coordinates !== undefined);
				assert.ok(res.body.pictures[0].provider !== undefined);
				assert.ok(res.body.pictures[0].author !== undefined);
				assert.ok(res.body.pictures[0].license !== undefined);
				assert.ok(res.body.pictures[0].detailsUrl.startsWith("http"));
				done();
			});
		}).timeout(15000);
		
		it("should return pictures on GET with BBox and time range defined", function(done){
			request(server)
			.get("/search/bbox?south=48.125&north=48.13&west=-1.69&east=-1.685&mindate=1495324800000&maxdate=1495411200000")
			.expect("Content-type",/json/)
			.expect(200)
			.end(function(err, res){
				assert.equal(res.status, 200);
				assert.equal(res.body.status, "OK");
				assert.equal(res.body.pictures.length, 31);
				assert.equal(res.body.pictures[0].pictureUrl, "https://d1cuyjsrcm0gby.cloudfront.net/eJMBmO6ndsHysDHsSHdhuQ/thumb-2048.jpg");
				assert.ok(res.body.pictures[0].date > 0);
				assert.ok(res.body.pictures[0].coordinates !== undefined);
				assert.equal(res.body.pictures[0].provider, "Mapillary");
				assert.equal(res.body.pictures[0].author, "panieravide");
				assert.equal(res.body.pictures[0].license, "CC By-SA 4.0");
				assert.equal(res.body.pictures[0].detailsUrl, "https://www.mapillary.com/app/?pKey=eJMBmO6ndsHysDHsSHdhuQ&lat=48.127471560000004&lng=-1.6850350500000104&focus=photo");
				done();
			});
		}).timeout(15000);
		
		it("should return no pictures on GET with BBox, time range and use defined over Rennes", function(done){
			request(server)
			.get("/search/bbox?south=48.125&north=48.13&west=-1.69&east=-1.685&mindate=1495324800000&maxdate=1495411200000&use=openstreetcam,wikicommons")
			.expect("Content-type",/json/)
			.expect(200)
			.end(function(err, res){
				assert.equal(res.status, 200);
				assert.equal(res.body.status, "OK");
				assert.equal(res.body.pictures.length, 0);
				done();
			});
		}).timeout(15000);
		
		it("should return no pictures on GET with BBox, time range and ignore defined over Rennes", function(done){
			request(server)
			.get("/search/bbox?south=48.125&north=48.13&west=-1.69&east=-1.685&mindate=1495324800000&maxdate=1495411200000&ignore=mapillary")
			.expect("Content-type",/json/)
			.expect(200)
			.end(function(err, res){
				assert.equal(res.status, 200);
				assert.equal(res.body.status, "OK");
				assert.equal(res.body.pictures.length, 0);
				done();
			});
		}).timeout(15000);
		
		it("should return filtered pictures on GET with BBox, time range and use defined over SF", function(done){
			request(server)
			.get("/search/bbox?south=37.77&north=37.775&west=-122.455&east=-122.45&mindate=1491091200000&maxdate=1491177600000&use=openstreetcam")
			.expect("Content-type",/json/)
			.expect(200)
			.end(function(err, res){
				assert.equal(res.status, 200);
				assert.equal(res.body.status, "OK");
				assert.equal(res.body.pictures.length, 20);
				for(const p of res.body.pictures) {
					assert.equal(p.provider, "OpenStreetCam");
				}
				done();
			});
		}).timeout(15000);
	});
	
	describe('/search/around', function() {
		it("should return NOTOK on GET without parameters", function(done){
			request(server)
			.get("/search/around")
			.expect("Content-type",/json/)
			.expect(500)
			.end(function(err, res){
				assert.equal(res.status, 500);
				assert.equal(res.body.status, "NOTOK");
				done();
			});
		});
		
		it("should return pictures on GET with coordinates + radius only", function(done){
			request(server)
			.get("/search/around?lat=48.125&lng=-1.69&radius=250")
			.expect("Content-type",/json/)
			.expect(200)
			.end(function(err, res){
				assert.equal(res.status, 200);
				assert.equal(res.body.status, "OK");
				assert.ok(res.body.pictures.length > 0);
				assert.ok(res.body.pictures[0].pictureUrl.startsWith("http"));
				assert.ok(res.body.pictures[0].date > 0);
				assert.ok(res.body.pictures[0].coordinates !== undefined);
				assert.ok(res.body.pictures[0].provider !== undefined);
				assert.ok(res.body.pictures[0].author !== undefined);
				assert.ok(res.body.pictures[0].license !== undefined);
				assert.ok(res.body.pictures[0].detailsUrl.startsWith("http"));
				done();
			});
		}).timeout(15000);
		
		it("should return pictures on GET with coordinates, radius, date parameters", function(done){
			request(server)
			.get("/search/around?lat=48.125&lng=-1.69&radius=250&mindate=1493596800000&maxdate=1498867200000")
			.expect("Content-type",/json/)
			.expect(200)
			.end(function(err, res){
				assert.equal(res.status, 200);
				assert.equal(res.body.status, "OK");
				assert.ok(res.body.pictures.length > 0);
				
				for(const p of res.body.pictures) {
					assert.ok(p.pictureUrl.startsWith("http"));
					assert.ok(p.date >= 1493596800000);
					assert.ok(p.date <= 1498867200000);
					assert.ok(p.coordinates !== undefined);
					assert.ok(p.provider !== undefined);
					assert.ok(p.author !== undefined);
					assert.ok(p.license !== undefined);
					assert.ok(p.detailsUrl.startsWith("http"));
				}
				
				done();
			});
		}).timeout(15000);
	});
	
	describe('/fetchers', function() {
		it("should return fetchers on GET", function(done){
			request(server)
			.get("/fetchers")
			.expect("Content-type",/json/)
			.expect(200)
			.end(function(err, res){
				assert.equal(res.status, 200);
				assert.equal(res.body.status, "OK");
				assert.equal(res.body.fetchers.mapillary.name, "Mapillary");
				assert.equal(res.body.fetchers.mapillary.homepageUrl, "https://www.mapillary.com/");
				done();
			});
		});
	});
	
	describe('/summary/bbox', function() {
		it("should return NOTOK on GET without parameters", function(done){
			request(server)
			.get("/summary/bbox")
			.expect("Content-type",/json/)
			.expect(500)
			.end(function(err, res){
				assert.equal(res.status, 500);
				assert.equal(res.body.status, "NOTOK");
				done();
			});
		});
		
		it("should return summary on GET with BBox only", function(done){
			request(server)
			.get("/summary/bbox?west=-1.69&south=48.125&east=-1.685&north=48.13")
			.expect("Content-type",/json/)
			.expect(200)
			.end(function(err, res){
				assert.equal(res.status, 200);
				assert.equal(res.body.status, "OK");
				assert.ok(res.body.summary !== undefined);
				assert.ok(!isNaN(res.body.summary.amount));
				assert.ok(!isNaN(res.body.summary.last));
				assert.ok(res.body.summary.approxAmount === true || res.body.summary.approxAmount === false);
				done();
			});
		}).timeout(10000);
		
		it("should return summary on GET with BBox and time range", function(done){
			request(server)
			.get("/summary/bbox?west=-1.69&south=48.125&east=-1.685&north=48.13&mindate=1483228800000&maxdate=1485907200000")
			.expect("Content-type",/json/)
			.expect(200)
			.end(function(err, res){
				assert.equal(res.status, 200);
				assert.equal(res.body.status, "OK");
				assert.ok(res.body.summary !== undefined);
				assert.equal(res.body.summary.amount, 36);
				assert.ok(res.body.summary.last >= 1483228800000 && res.body.summary.last <= 1485907200000);
				assert.equal(res.body.summary.approxAmount, false);
				done();
			});
		}).timeout(10000);
	});
});
